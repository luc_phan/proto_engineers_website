from fabric.api import *

def upgrade():
    with settings(sudo_user="proto"):
        with cd("/home/proto/live/proto_engineers_website"):
            sudo("git pull")
            with prefix("source virtualenvwrapper.sh; workon local"):
                sudo("python manage.py collectstatic --noinput")
    run("sudo systemctl restart proto")
