from django.db import models

# Create your models here.

class User(models.Model):
    name = models.CharField(max_length=16, unique=True)
    def __str__(self):
        return 'User(name={})'.format(self.name)

class UserHistory(models.Model):
    user = models.ForeignKey(User)
    ip = models.GenericIPAddressField()
    last_seen = models.DateTimeField(auto_now=True)

class Session(models.Model):
    session_key = models.CharField(max_length=32, unique=True)
    user = models.ForeignKey(User, null=True)
    def __str__(self):
        return 'Session(session_key={})'.format(self.session_key)

class SessionHistory(models.Model):
    session = models.ForeignKey(Session)
    ip = models.GenericIPAddressField()
    last_seen = models.DateTimeField(auto_now=True)
