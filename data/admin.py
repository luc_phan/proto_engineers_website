from django.contrib import admin
from .models import Session, SessionHistory, User, UserHistory

# Register your models here.

class SessionAdmin(admin.ModelAdmin):
    list_display = ['session_key', 'user']

class SessionHistoryAdmin(admin.ModelAdmin):
    list_display = ['session', 'ip', 'last_seen']

class UserAdmin(admin.ModelAdmin):
    list_display = ['name']

class UserHistoryAdmin(admin.ModelAdmin):
    list_display = ['user', 'ip', 'last_seen']

admin.site.register(Session, SessionAdmin)
admin.site.register(SessionHistory, SessionHistoryAdmin)
admin.site.register(User, UserAdmin)
admin.site.register(UserHistory, UserHistoryAdmin)
