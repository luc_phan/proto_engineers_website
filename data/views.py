from .models import Session, SessionHistory, User, UserHistory
from rest_framework import viewsets, filters
from .serializers import SessionSerializer, SessionHistorySerializer, UserSerializer, UserHistorySerializer

# Create your views here.

class SessionViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows sessions to be viewed or edited.
    """
    queryset = Session.objects.all()
    serializer_class = SessionSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filter_fields = serializer_class.Meta.fields

class SessionHistoryViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows session history to be viewed or edited.
    """
    queryset = SessionHistory.objects.all()
    serializer_class = SessionHistorySerializer

class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer

class UserHistoryViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = UserHistory.objects.all()
    serializer_class = UserHistorySerializer
