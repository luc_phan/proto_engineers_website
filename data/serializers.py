from rest_framework import serializers
from .models import Session, User, SessionHistory, UserHistory

class SessionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Session
        fields = ('id', 'session_key', 'user')

class SessionHistorySerializer(serializers.ModelSerializer):
    class Meta:
        model = SessionHistory
        fields = ('id', 'session', 'ip', 'last_seen')

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'name',)

class UserHistorySerializer(serializers.ModelSerializer):
    class Meta:
        model = UserHistory
        fields = ('id', 'user', 'ip', 'last_seen')
