# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.utils.timezone import utc
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('data', '0003_session_user'),
    ]

    operations = [
        migrations.AddField(
            model_name='session',
            name='ip',
            field=models.GenericIPAddressField(null=True),
        ),
        migrations.AddField(
            model_name='session',
            name='last_seen',
            field=models.DateTimeField(auto_now=True, default=datetime.datetime(2015, 6, 8, 12, 55, 41, 17072, tzinfo=utc)),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='user',
            name='ip',
            field=models.GenericIPAddressField(null=True),
        ),
        migrations.AddField(
            model_name='user',
            name='last_seen',
            field=models.DateTimeField(auto_now=True, default=datetime.datetime(2015, 6, 8, 12, 56, 1, 949195, tzinfo=utc)),
            preserve_default=False,
        ),
    ]
