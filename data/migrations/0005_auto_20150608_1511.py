# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('data', '0004_auto_20150608_1456'),
    ]

    operations = [
        migrations.CreateModel(
            name='SessionHistory',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
                ('ip', models.GenericIPAddressField()),
                ('last_seen', models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.CreateModel(
            name='UserHistory',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
                ('ip', models.GenericIPAddressField()),
                ('last_seen', models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.RemoveField(
            model_name='session',
            name='ip',
        ),
        migrations.RemoveField(
            model_name='session',
            name='last_seen',
        ),
        migrations.RemoveField(
            model_name='user',
            name='ip',
        ),
        migrations.RemoveField(
            model_name='user',
            name='last_seen',
        ),
        migrations.AddField(
            model_name='userhistory',
            name='user',
            field=models.ForeignKey(to='data.User'),
        ),
        migrations.AddField(
            model_name='sessionhistory',
            name='session',
            field=models.ForeignKey(to='data.Session'),
        ),
    ]
