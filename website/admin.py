from django.contrib import admin
from django.contrib.sessions.models import Session
from .models import IpAddress

# Register your models here.

class SessionAdmin(admin.ModelAdmin):
    def _session_data(self, obj):
        return obj.get_decoded()
    list_display = ['session_key', '_session_data', 'expire_date']

class IpAddressAdmin(admin.ModelAdmin):
    list_display = ('ip', 'count', 'last_seen')
    readonly_fields = ('last_seen',)

admin.site.register(Session, SessionAdmin)
admin.site.register(IpAddress, IpAddressAdmin)
