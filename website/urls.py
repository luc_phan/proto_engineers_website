from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.home, name='home'),
    url(r'^home.js$', views.home_js, name='home_js'),
    url(r'^about$', views.about, name='about'),
    url(r'^contact$', views.contact, name='contact'),
]
