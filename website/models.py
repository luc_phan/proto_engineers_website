from django.db import models

# Create your models here.

class IpAddress(models.Model):
    ip = models.GenericIPAddressField(unique=True)
    count = models.IntegerField(default=1)
    last_seen = models.DateTimeField(auto_now=True)
    def __str__(self):
        return 'IpAddress(ip={})'.format(self.ip)

from pipeline.signals import js_compressed
from django.conf import settings
import os

def clear_files(sender, **kwargs):
    if 'package' in kwargs:
        for source in kwargs['package'].sources:
            path = os.path.join(settings.STATIC_ROOT, source)
            print('Removing: {}'.format(path))
            os.remove(path)

js_compressed.connect(clear_files)
