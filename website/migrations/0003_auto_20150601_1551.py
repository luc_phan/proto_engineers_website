# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('website', '0002_ipaddress'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ipaddress',
            name='count',
            field=models.IntegerField(default=1),
        ),
    ]
