# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('website', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='IpAddress',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, primary_key=True, serialize=False)),
                ('ip', models.GenericIPAddressField(unique=True)),
                ('count', models.IntegerField(default=0)),
                ('last_seen', models.DateTimeField(auto_now=True)),
            ],
        ),
    ]
