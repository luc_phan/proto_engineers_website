from .models import IpAddress

def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip

class IpAddressStats:
    def process_request(self, request):
        ip = get_client_ip(request)
        ips = IpAddress.objects.filter(ip=ip)
        if ips:
            ip_address = ips[0]
            ip_address.count += 1
        else:
            ip_address = IpAddress(ip=ip)
        ip_address.save()

    def process_response(self, request, response):
        return response
