{% load staticfiles %}

var g = $('.game').game({
  url: '{{ url }}',
  session_key: '{{ request.session.session_key }}',
  template: '{% static "website/game.mst" %}',
  images: {
    grass: {
      url: '{% static "website/grass.png" %}',
      hotspot_x: 0,
      hotspot_y: 0,
    },
    man: {
      url: '{% static "website/man.png" %}',
      hotspot_x: 24,
      hotspot_y: 104,
    }
  },
  tiles: {
    1: 'grass'
  },
  debug: {% if debug %}true{% else %}false{% endif %}
});

//g.game('init');
//g.load_resources();
$(document).ready(function() {
  g.game('go');
});
