window.requestAnimFrame = ( function() {
  return window.requestAnimationFrame       ||
         window.webkitRequestAnimationFrame ||
         window.mozRequestAnimationFrame    ||
         window.oRequestAnimationFrame      ||
         window.msRequestAnimationFrame     ||
         function(callback, element) {
           window.setTimeout(callback, 1000 / 60);
         };
})();

function distance( x1, y1, x2, y2 ) {
  return Math.sqrt( Math.pow(x2-x1,2) + Math.pow(y2-y1,2) );
}

function Room(data) {
  this.name = data.name;
  this.category = data.category;
  this.players = data.players;
  this.field = data.field;
  this.units = data.units;
}

Room.prototype = {
  select_unit: function( x, y ) {
    var units = this._select_units( x, y );
    var unit = units.reduce(function(min_unit, unit) {
      if( !min_unit ) {
        min_unit = unit;
      }
      else {
        var min_center = min_unit.get_center();
        var min_dist = distance( x, y, min_center.x, min_center.y );
        var center = unit.get_center();
        var dist = distance( x, y, center.x, center.y );
        if( dist < min_dist ) {
          min_unit = unit;
        }
      }
      return min_unit;
    }, null);
    return unit;
  },
  _select_units: function( x, y ) {
    var units = [];
    for( var i in this.units ) {
      var unit = this.units[i];
      var box = unit.get_box();
      if(
        box.left <= x && x <= box.right && box.top <= y && y <= box.bottom
      ) {
        units.push( unit );
      }
    }
    return units;
  }
}

function Player(data) {
  this.id = data.id;
  this.name = data.name;
  this.connected = data.connected;
}

function Field(data) {
  this.width_pixels = data.width_pixels;
  this.height_pixels = data.height_pixels;
  this.width_tiles = data.width_tiles;
  this.height_tiles = data.height_tiles;
  this.map = data.map;
}

function Unit(data) {
  this.id = data.id;
  this.category = data.category;
  this.x = data.x;
  this.y = data.y;
  this.player_id = data.player_id;
  this.picture = data.picture;
}

Unit.prototype = {
  get_box: function() {
    var picture = this.picture;
    var x = this.x - picture.hotspot_x;
    var y = this.y - picture.hotspot_y;
    return {
      left: x,
      top: y,
      right: x + picture.image.width,
      bottom: y + picture.image.height
    };
  },
  get_center: function() {
    var picture = this.picture;
    var x = this.x - picture.hotspot_x;
    var y = this.y - picture.hotspot_y;
    return {
      x: x + picture.image.width / 2,
      y: y + picture.image.height / 2
    }
  }
}

function Picture( image, hotspot_x, hotspot_y ) {
  this.image = image;
  this.hotspot_x = hotspot_x;
  this.hotspot_y = hotspot_y;
}

Picture.prototype = {
  draw: function( context, x, y ) {
    context.drawImage( this.image, x-this.hotspot_x, y-this.hotspot_y );
  }
}

$.widget( 'quoonel.game', {

  options: {
    debug: false,
    chat_input_length: 128
  },
  _status: null,
  _template: null,
  _websocket: null,
  _player: null,
  _room: {},
  _canvas: {
    x: 0,
    y: 0,
    tile_size: 64,
    width_tiles: 16,
    height_tiles: 9
  },
  _inputs: {},
  _pictures: {},
  _tiles: {},
  _selected: {},

  _log: function() {
    if (this.options.debug) {
      console.log.apply( console, arguments );
    }
  },

  _create: function() {
  },

  _initialize: function() {
    var game = this;
    this.element.on("template_loaded", function() {
      game._render_layout();
      game._init_canvas();
      game._init_chat();
    });
    this._load_template();
    this._init_pictures();
    this._init_tiles();
    this._init_inputs();
  },

  _load_template: function() {
    var game = this;
    $.get( this.options.template, function(template) {
      game._template = template;
      game.element.trigger("template_loaded");
    });
  },

  _render_layout: function() {
    var rendered = Mustache.render(
      this._template,
      {
        chat_input_length: this.options.chat_input_length
      }
    );
    this.element.html(rendered);
    this.element.trigger("layout_rendered");
  },

  _init_canvas: function() {
    var game = this;
    this._set_canvas_size();
    this._resize_canvas();
    $(window).resize(function() {
      game._resize_canvas();
    });
    var $canvas = this.element.find('canvas');
    $canvas.click( function(e) {
      var rect = $canvas[0].getBoundingClientRect();
      var x = e.clientX - rect.left;
      var y = e.clientY - rect.top;
      var mx = x * $canvas.attr('width') / $canvas.width() + game._canvas.x;
      var my = y * $canvas.attr('height') / $canvas.height() + game._canvas.y;
      var unit = game._room.select_unit( mx, my );
      if( unit ) {
        game.select( unit );
      }
    });
    $canvas.mousedown( function(e) {
      if( e.which == 3 ) {
        game._log( e.which );
      }
    });
    $canvas.bind( "contextmenu", function(e) {
      e.preventDefault();
    });
  },

  _set_canvas_size: function() {
    var canvas = this._canvas;
    var tile_size = canvas.tile_size;
    var $canvas = this.element.find('canvas');
    $canvas.attr('width', canvas.width_tiles*tile_size);
    $canvas.attr('height', canvas.height_tiles*tile_size);
  },

  _resize_canvas: function() {
    var section = this.element.find('section.field');
    var canvas = this.element.find('canvas');
    var sw = section.width();
    var sh = section.height();
    var w, h;
    var left = 0;
    var t = 0;
    if ( sw / sh >= 16 / 9 ) {
      h = sh;
      w = h * 16 / 9;
      left = (sw-w)/2;
    }
    else {
      w = section.width();
      h = w * 9 / 16;
      t = (sh-h)/2;
    }
    canvas.width(w);
    canvas.height(h);
    canvas.css({ left:left, top:t });
  },

  _init_chat: function() {
    var game = this;
    this.element.find('.chat form').submit( function(e) {
      var input = game.element.find('.chat input');
      if( input.val() ) {
        game._send({ type: 'chat', text: input.val() });
        input.val('');
      }
      e.preventDefault();
    });
  },

  _init_pictures: function() {
    var images = this.options.images;
    for( var name in images ) {
      var i = images[name];
      var image = new Image;
      image.src = i.url;
      this._pictures[name] = new Picture( image, i.hotspot_x, i.hotspot_y );
    }
  },

  _init_tiles: function() {
    var tiles = this.options.tiles;
    for( var i in tiles ) {
      var name = tiles[i];
      this._tiles[i] = this._pictures[name];
    }
  },

  _init_inputs: function() {
    var game = this;
    $(document).keydown( function(e) {
      game._inputs[e.which] = true;
    });

    $(document).keyup( function(e) {
      game._inputs[e.which] = false;
    });
  },

  go: function() {
    var game = this;
    this.element.on("room_loaded", function() {
      game._animate();
    });
    this.element.on("layout_rendered", function() {
      game._start_socket();
    });
    this._initialize();
  },

  _start_socket: function() {
    var game = this;
    var options = this.options;
    var url = atob( options.url );
    var s = new WebSocket( url );
    this._websocket = s;
    s.onerror = function (e) {
      game._add_message( "*** WebSocket error.", "error" );
    };
    s.onopen = function() {
      game._add_message( "*** Connected!", "success" );
      game._send({ type: 'login', session_key: options.session_key });
    };
    s.onclose = function() {
      game._add_message( "*** Disconnected!", "error" );
    };
    s.onmessage = function(e) {
      var data = JSON.parse( e.data );
      if (game.options.debug) {
        game._log( data );
      }
      switch (data.type) {
        case 'logged':
          game._add_message( "*** Logged: " + data.player.name, "success" );
          game._player = data.player;
          break;
        case 'room':
          var players = data.room.players;
          var field = data.room.field;
          var units = data.room.units;
          game._room = new Room({
            name: data.room.name,
            category: data.room.category,
            players: Object.keys(players).reduce(function(_players, id) {
              var p = players[id];
              _players[id] = new Player(p);
              return _players;
            }, {}),
            field: new Field({
              width_pixels: field.width_pixels,
              height_pixels: field.height_pixels,
              width_tiles: field.width_tiles,
              height_tiles: field.height_tiles,
              map: field.map
            }),
            units: Object.keys(units).reduce(function(_units, id) {
              var u = units[id];
              u.picture = game._pictures['man'];
              _units[id] = new Unit(u);
              return _units;
            }, {})
          });
          game.element.trigger("room_loaded");
          break;
        case 'joined':
          game._add_message( "*** Joined: " + data.player.name, "joined" );
          game._room.players[data.player.id] = new Player(data.player);
          break;
        case 'disconnected':
          game._add_message( "*** Disconnected: " + data.player.name, "disconnected" );
          var player = game._room.players[data.player.id];
          player.connected = false;
          break;
        case 'chat':
          game._add_message( data.text, "chat", data.playername );
          break;
        case 'new_unit':
          data.unit.picture = game._pictures['man'];
          game._room.units[data.unit.id] = new Unit(data.unit);
          break;
        case 'error':
          game._add_message( "*** " + data.message, "error" );
          break;
        default:
          console.log( "*** Unknown message type: " + data.type );
      }
    };
  },

  _send: function(data) {
    var json = JSON.stringify(data);
    var s = this._websocket;
    s.send(json);
  },

  _add_message: function(text, type, playername) {
    var message = $('<div class="message">');
    if ( type == 'chat' ) {
      var nickname = $('<span>').text(playername);
      if ( this._player.name == playername ) {
        nickname.addClass('own_nickname');
      }
      else {
        nickname.addClass('nickname');
      }
      var text = $('<span class="text">').text(text);
      message.append('<', nickname, '> ', text);
    }
    else {
      message.text(text);
      if (type) {
        message.addClass(type);
      }
    }
    var messages = this.element.find('.chat section.messages');
    messages.append( message );
    messages.scrollTop( messages.prop("scrollHeight") );
  },

  _animate: function() {
    var game = this;
    requestAnimFrame( function() {
      game._animate();
    });
    var game = this;
    var speed = 8;
    if (game._inputs[39]) {
      game._canvas.x += speed;
    }
    if (game._inputs[37]) {
      game._canvas.x -= speed;
    }
    if (game._inputs[40]) {
      game._canvas.y += speed;
    }
    if (game._inputs[38]) {
      game._canvas.y -= speed;
    }
    game._draw();
  },

  _draw: function() {
    var canvas = this._canvas;
    var x = canvas.x;
    var y = canvas.y;
    var $canvas = this.element.find('canvas');
    var context = $canvas[0].getContext("2d");
    context.clearRect(0, 0, $canvas.attr("width"), $canvas.attr("height"));
    context.save()
    context.translate( -x, -y );
    this._display_field(context);
    this._display_sprites(context);
    context.restore();
  },

  _display_field: function(context) {
    var canvas = this._canvas;
    var x = canvas.x;
    var y = canvas.y;
    var tile_size = canvas.tile_size;
    var starting_col = Math.floor( x / tile_size );
    var starting_row = Math.floor( y / tile_size );
    var ending_col = starting_col + canvas.width_tiles;
    var ending_row = starting_row + canvas.height_tiles;
    var field = this._room.field;
    var map = field.map;
    for( var row = starting_row; row <= ending_row; row++ ) {
      for( var col = starting_col; col <= ending_col; col++ ) {
        if(
          0 <= row &&
          row < field.height_tiles &&
          0 <= col &&
          col <= field.width_tiles
        ) {
          var i = map[row][col];
          var tile = this._tiles[i];
          if( tile ) {
            tile.draw( context, col*tile_size, row*tile_size );
          }
        }
      }
    }
  },

  _display_sprites: function(context) {
    var picture = this._pictures['man'];
    var units = this._room.units;
    var ids = Object.keys(units);
    for(
      var i in ids.sort( function(a,b) { return units[a].y - units[b].y } )
    ) {
      var unit_id = ids[i];
      var unit = units[unit_id];
      var x = unit.x;
      var y = unit.y;
      var id = unit.player_id;
      var player = this._room.players[id];

      /*
      context.beginPath();
      context.moveTo( x-128, y );
      context.lineTo( x+128, y );
      context.moveTo( x, y-128 );
      context.lineTo( x, y+128 );
      context.strokeStyle = '#60ff60';
      context.stroke();
      */

      picture.draw( context, x, y );
      context.font = "25px serif";
      if (player.connected) {
        context.fillStyle = "white";
      }
      else {
        context.fillStyle = "#ffa0a0";
      }
      var metrics = context.measureText(player.name);
      var width = metrics.width;
      context.fillText( player.name, x-width/2, y-picture.image.height );

      if (this._selected[unit.id]) {
        var box = unit.get_box();
        context.beginPath();
        context.strokeStyle = "#60ff60";
        context.rect(
          box.left,
          box.top,
          box.right - box.left,
          box.bottom - box.top
        );
        context.stroke();
      }
    }
  },

  select: function( unit ) {
    this._selected = {};
    this._selected[unit.id] = unit;
  }

});
