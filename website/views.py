from django.shortcuts import render
from django.conf import settings
import random
import base64

# Create your views here.

def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip

def home(request):
    request.session['ip'] = get_client_ip(request)
    return render(
        request,
        'website/home.html'
    )

def home_js(request):
    url = settings.WEBSOCKETS.get(request.get_host())
    if not url:
        url = settings.WEBSOCKETS.get('default')
    return render(
        request,
        'website/home.js', {
            'url': base64.b64encode(bytes(url, 'UTF-8')),
            'debug': settings.DEBUG
        }
    )

def about(request):
    return render(request, 'website/about.html')

def contact(request):
    return render(request, 'website/contact.html')
