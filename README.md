### Installation ###

```
$ cat /etc/system-release
CentOS Linux release 7.0.1406 (Core)
```

Python installation :

```
$ sudo yum install http://dl.iuscommunity.org/pub/ius/stable/CentOS/7/x86_64/ius-release-1.0-13.ius.centos7.noarch.rpm
$ sudo yum install python34u
```

Production user :

```
$ sudo useradd proto
$ sudo su proto
```

Python environment :

```
$ sudo pip3 install virtualenv
$ sudo pip3 install virtualenvwrapper
$ echo export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3 >> ~/.bashrc
$ echo source /usr/bin/virtualenvwrapper.sh >> ~/.bashrc
$ source ~/.bashrc
$ mkvirtualenv local -p /usr/bin/python3
```

Cloning :

```
$ ssh-keygen
$ cat ~/.ssh/id_rsa.pub
$ echo "Add SSH key to Bitbucket"
$ mkdir ~/live
$ cd ~/live
$ git clone 'git@bitbucket.org:luc_phan/proto_engineers_website.git'
```

Website settings :

```
$ cd ~/live/proto_engineers_website/proto_engineers_website
$ vim settings.py
```
```python
#DEBUG = True
DEBUG = False
```

Dependencies :

```
$ pip install django
$ pip install django-pipeline
$ pip install djangorestframework
$ pip install django-filter
```

Database initialization :

```
$ cd ~/live/proto_engineers_website
$ python manage.py migrate
```

uWSGI Installation :

```
$ sudo yum install python34u-devel
$ sudo yum install gcc
$ pip install uwsgi
$ cd ~/live/proto_engineers_website
$ vim proto.ini
```

```
#!ini
# proto.ini file
[uwsgi]

# Django-related settings
# the base directory (full path)
chdir           = /home/proto/live/proto_engineers_website
# Django's wsgi file
module          = proto_engineers_website.wsgi
# the virtualenv (full path)
home            = /home/proto/.virtualenvs/local

# process-related settings
# master
master          = true
# maximum number of worker processes
processes       = 10
# the socket (use the full path to be safe
socket          = /home/proto/live/proto_engineers_website/website.sock
# ... with appropriate permissions - may be needed
chmod-socket    = 664
# clear environment on exit
vacuum          = true
```

uWSGI service :

```
$ sudo vim /etc/systemd/system/proto.service
```

```ini
; proto.service file
[Unit]
Description=Proto-Engineers
After=network.target

[Service]
User=proto
Group=proto
WorkingDirectory=/home/proto/live/proto_engineers_website
Environment="PATH=/home/proto/.virtualenvs/local/bin"
ExecStart=/home/proto/.virtualenvs/local/bin/uwsgi --ini proto.ini

[Install]
WantedBy=multi-user.target
```
```
$ sudo service proto start
$ sudo systemctl enable proto
```

Nginx :

```bash
$ sudo yum install nginx
$ sudo vim /etc/nginx/conf.d/proto.conf
```
```nginx
# proto.conf file

# the upstream component nginx needs to connect to
upstream django {
    server unix:///home/proto/live/proto_engineers_website/website.sock; # for a file socket
    #server 127.0.0.1:8001; # for a web port socket (we'll use this first)
}

# configuration of the server
server {
    # the port your site will be served on
    #listen      25380 ssl;
    listen      25380;
    # the domain name it will serve for
    #server_name .example.com; # substitute your machine's IP address or FQDN
    charset     utf-8;

    #ssl_certificate /etc/nginx/ssl/ca.crt;
    #ssl_certificate_key /etc/nginx/ssl/ca.key;

    # max upload size
    client_max_body_size 75M;   # adjust to taste

    # Django media
    location /media  {
        #alias /path/to/your/mysite/media;  # your Django project's media files - amend as required
    }

    location /static {
        alias /home/proto/live/proto_engineers_website/static; # your Django project's static files - amend as required
    }

    # Finally, send all non-media requests to the Django server.
    location / {
        uwsgi_pass  django;
        #include     /path/to/your/mysite/uwsgi_params; # the uwsgi_params file you installed
        include     uwsgi_params; # the uwsgi_params file you installed
    }
}
```

Static files :

```
$ sudo yum install npm
$ sudo npm -g install yuglify
$ cd ~/live/proto_engineers_website
$ python manage.py collectstatic
$ sudo usermod -a -G proto nginx
$ chmod g+x /home/proto
$ sudo service nginx start
$ sudo systemctl enable nginx
```

Firewall :

```
$ sudo firewall-cmd --zone=public --add-port=25380/tcp --permanent
$ sudo firewall-cmd --reload
$ sudo firewall-cmd --list-ports
```

### Development ###

```
$ cat /etc/system-release
CentOS Linux release 7.0.1406 (Core)
```

Python installation :

```
$ sudo yum install http://dl.iuscommunity.org/pub/ius/stable/CentOS/7/x86_64/ius-release-1.0-13.ius.centos7.noarch.rpm
$ sudo yum install python34u
```

Python environment :

```
$ sudo pip3 install virtualenv
$ sudo pip3 install virtualenvwrapper
$ echo export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3 >> ~/.bashrc
$ echo source /usr/bin/virtualenvwrapper.sh >> ~/.bashrc
$ source ~/.bashrc
$ mkvirtualenv local -p /usr/bin/python3
```

Cloning :

```
$ ssh-keygen
$ cat ~/.ssh/id_rsa.pub
$ echo "Add SSH key to Bitbucket"
$ mkdir ~/work
$ cd ~/work
$ git clone 'git@bitbucket.org:luc_phan/proto_engineers_website.git'
```

Debug switch :

```
$ cd ~/live/proto_engineers_website
$ touch debug
```

Dependencies :

```
$ pip install django
```

Database initialization :

```
$ cd ~/work/proto_engineers_website
$ python manage.py migrate
```

Firewall :

```
$ sudo firewall-cmd --zone=public --add-port=8000/tcp --permanent
$ sudo firewall-cmd --reload
$ sudo firewall-cmd --list-ports
```

Testing :

```
$ cd ~/work/proto_engineers_website
$ python manage.py runserver 0.0.0.0:8000
```